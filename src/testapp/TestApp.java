/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author alexxx
 */
public class TestApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NeuronNet net = new NeuronNet(12, 7);
        Shape zero = new Shape(2, 2, 3);
            zero.setPixel(0, 0, 0);
            zero.setPixel(1, 0, 0);
            zero.setPixel(1, 1, 0);
        Shape one = new Shape(2, 2, 3);
            one.setPixel(0, 0, 0);
            one.setPixel(1, 0, 0);
            one.setPixel(1, 1, 0);
            one.setPixel(1, 1, 1);
        Shape two = new Shape(2, 2, 3);
            two.setPixel(0, 0, 1);
            two.setPixel(1, 0, 1);
            two.setPixel(1, 1, 0);
            two.setPixel(1, 1, 1);
        Shape three = new Shape(2, 2, 3);
            three.setPixel(0, 0, 1);
            three.setPixel(1, 0, 0);
            three.setPixel(1, 0, 1);
            three.setPixel(1, 1, 1);
        Shape four = new Shape(2, 2, 3);
            four.setPixel(0, 0, 0);
            four.setPixel(0, 0, 1);
            four.setPixel(0, 0, 2);
            four.setPixel(1, 0, 0);
        Shape five = new Shape(2, 2, 3);
            five.setPixel(0, 0, 1);
            five.setPixel(1, 0, 0);
            five.setPixel(1, 0, 1);
            five.setPixel(1, 0, 2);
        Shape six = new Shape(2, 2, 3);
            six.setPixel(0, 0, 1);
            six.setPixel(0, 0, 2);
            six.setPixel(1, 0, 0);
            six.setPixel(1, 0, 1);
            
         ArrayList<Shape> shapes = new ArrayList<Shape>();
             shapes.add(zero);
             shapes.add(one);
             shapes.add(two);
             shapes.add(three);
             shapes.add(four);
             shapes.add(five);
             shapes.add(six);
             
        for (int i = 0; i < 7; i++) {
            Shape curr = shapes.get(i);
            while (net.getAnswer(curr.getLine()) != i) {
                for (int j = 0; j < 4; j++) {
                    net.train(i, curr.getLine());
                    curr.rotateCW(1);
                }
                for (int j = 0; j < 4; j++) {
                    net.train(i, curr.getLine());
                    curr.rotateCW(2);
                }
                for (int j = 0; j < 4; j++) {
                    net.train(i, curr.getLine());
                    curr.rotateCW(3);
                }
                for (int j = 0; j < 4; j++) {
                    net.train(i, curr.getLine());
                    curr.rotateCCW(1);
                }
                for (int j = 0; j < 4; j++) {
                    net.train(i, curr.getLine());
                    curr.rotateCCW(2);
                }
                for (int j = 0; j < 4; j++) {
                    net.train(i, curr.getLine());
                    curr.rotateCCW(3);
                }
            }
        }
        
        for (int i = 0; i < 7; i++)
            System.out.println(i +") "+net.getAnswer(shapes.get(i).getLine()));
    }
}
