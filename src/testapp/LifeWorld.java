/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class LifeWorld {
    private ArrayList<ArrayList<Boolean>> world;
    private int sizeX;
    private int sizeY;
        
    public LifeWorld(int x, int y) {
        this.sizeX = x;
        this.sizeY = y;
        this.world = new ArrayList<ArrayList<Boolean>>();
        for (int i = 0; i < this.sizeX; i++){
            this.world.add(new ArrayList<Boolean>());
            for ( int j = 0; j < this.sizeY; j++)
                this.world.get(i).add(Boolean.FALSE);
        }
    }
    
    public int getNeightboursCount(int currX, int currY) {
        //System.out.println(currX+" "+currY);
        int result = 0;
        ArrayList<int[]> coordinats = new ArrayList<int[]>();
        int[] cur = new int[2];
        cur[0] = currX-1;
        cur[1] = currY-1;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        cur = new int[2];
        cur[0] = currX;
        cur[1] = currY-1;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        cur = new int[2];
        cur[0] = currX+1;
        cur[1] = currY-1;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        cur = new int[2];
        cur[0] = currX-1;
        cur[1] = currY;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        cur = new int[2];
        cur[0] = currX+1;
        cur[1] = currY;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        cur = new int[2];
        cur[0] = currX-1;
        cur[1] = currY+1;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        cur = new int[2];
        cur[0] = currX;
        cur[1] = currY+1;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        cur = new int[2];
        cur[0] = currX+1;
        cur[1] = currY+1;
        if ((cur[0] >= 0 && cur[0] <= this.sizeX-1) &&
            (cur[1] >= 0 && cur[1] <= this.sizeY-1))
            coordinats.add(cur);
        for (int i = 0; i < coordinats.size(); i++) {
            cur = coordinats.get(i);
            if (get(cur[0], cur[1]))
                result++;
        }
        return result;
    }
    
    public void set(int x, int y, boolean state) {
        this.world.get(x).set(y, state);
    }
    
    public boolean get (int x, int y){
        return this.world.get(x).get(y);
    }
    
    @Override
    public LifeWorld clone() {
        LifeWorld result = new LifeWorld(this.sizeX, this.sizeY);
        for (int i = 0; i < this.sizeX; i++){
            for (int j = 0; j < this.sizeY; j++)
                result.set(i, j, get(i, j));
        }
        return result;
    }
    
    public void clear() {
        this.world = new ArrayList<ArrayList<Boolean>>();
        for (int i = 0; i < this.sizeX; i++){
            this.world.add(new ArrayList<Boolean>());
            for ( int j = 0; j < this.sizeY; j++)
                this.world.get(i).add(Boolean.FALSE);
        }
    }
    
    public int sizeX(){
        return this.sizeX;
    }
    
    public int sizeY(){
        return this.sizeY;
    }
    
    public int count(){
        int result = 0;
        for ( int i = 0; i < this.sizeX; i++){
            for (int j = 0; j < this.sizeY; j++)
                if (get(i, j))
                    result++;
        }
        return result;
    }
    
    public void print(){
        System.out.println();
        for ( int i = 0 ; i < this.sizeX; i++){
            System.out.println();
            for (int j = 0; j < this.sizeY; j++)
                if (get(i, j))
                    System.out.print(1);
                else
                    System.out.print(0);
        }
        System.out.println();
    }
}
