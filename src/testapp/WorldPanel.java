/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JPanel;

/**
 *
 * @author alexxx
 */
public class WorldPanel extends JPanel{
    public LifeWorld world;
    int elemSize = 20;
    int elemSpace = 3;
    
    public WorldPanel(){
        super();
        this.addMouseListener(new ClickListeners(this));
    }
    
    @Override
    public void paint(Graphics g){
        super.paint(g);
        if (this.world != null){
            for ( int i = 0; i < this.world.sizeX(); i++){
                for (int j = 0; j < this.world.sizeY(); j++) {
                    if (this.world.get(i, j))
                        g.fillRoundRect(this.elemSpace+(this.elemSpace+this.elemSize)*i, this.elemSpace+(this.elemSpace+this.elemSize)*j, 
                                        this.elemSize, this.elemSize, 
                                        this.elemSpace, this.elemSpace);
                    else
                        g.drawRoundRect(this.elemSpace+(this.elemSpace+this.elemSize)*i, this.elemSpace+(this.elemSpace+this.elemSize)*j, 
                                        this.elemSize, this.elemSize, 
                                        this.elemSpace, this.elemSpace);
                }
            }
        }
    }
    
    public void toggleElement(int x, int y){
        x -= this.elemSpace;
        y -= this.elemSpace;
        int xIndex = x /(this.elemSpace+this.elemSize);
        int yIndex = y /(this.elemSpace+this.elemSize);
        this.world.set(xIndex, yIndex, !this.world.get(xIndex, yIndex));
    }
    
    class ClickListeners implements MouseListener{
        public WorldPanel parent;
        public Timer timer;
        
        public ClickListeners(WorldPanel panel){
            this.parent = panel;
            timer = new Timer();
        }
        
        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getButton() == e.BUTTON1) {
                int x = e.getX();
                int y = e.getY();
                this.parent.toggleElement(x,y);
                this.parent.updateUI();
            }
            if (e.getButton() == e.BUTTON2) {
                this.parent.world.clear();
                timer.cancel();
                this.parent.updateUI();
            }
            if (e.getButton() == e.BUTTON3) {
                MyTimerTask timerTask = new MyTimerTask(parent);
                timer.schedule(timerTask, 0, 500);
            }
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void mousePressed(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void mouseExited(MouseEvent e) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }
    }
    
    class MyTimerTask extends TimerTask{ 
        public WorldPanel parent;
        
        public MyTimerTask(WorldPanel parent) {
            this.parent = parent;
        }
        
        @Override
        public void run() {
            LifeEngine engine = new LifeEngine(this.parent.world);
            for (int i = 0; i < 1; i++){                           
                engine.step();
                this.parent.updateUI();
            }
        }                
    }
}
