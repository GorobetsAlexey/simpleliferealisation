/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class Shape {
    public ArrayList< ArrayList< ArrayList<Integer> > > shape3D = new ArrayList<ArrayList<ArrayList<Integer>>>();
    public int x;
    public int y;
    public int z;
    
    public Shape(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
        for (int i = 0; i < x; i++) {
            this.shape3D.add(new ArrayList<ArrayList<Integer>>());
            for (int j = 0; j < y; j++){
                this.shape3D.get(i).add(new ArrayList<Integer>());
                for(int k = 0; k < z; k++)
                    this.shape3D.get(i).get(j).add(0);
            }
        }
    }
    
    public void setPixel (int i, int j, int k){
        this.shape3D.get(i).get(j).set(k, 1);
    }
    
    public ArrayList<Integer> getLine(){
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < this.shape3D.size(); i++) {
            for (int j = 0; j < this.shape3D.get(i).size(); j++){
                for(int k = 0; k < this.shape3D.get(i).get(j).size(); k++)
                    result.add(this.shape3D.get(i).get(j).get(k));
            }
        }
        return result;
    }
    
    public void rotateCW(int axis){
        
        switch(axis){
            case 1:
                Shape newShape = new Shape(this.y, this.x, this.z);
                for (int i = 0; i < this.x; i++) 
                    for (int j = 0; j < this.y; j++)
                        for (int k = 0; k < this.z; k++)
                            if (this.shape3D.get(i).get(j).get(k)== 1)
                                newShape.setPixel(j, i, k);
                for (int i = 0; i < newShape.x; i++) {
                    int p = 0;
                    for (int j = 0; j < newShape.y / 2; j++){
                        ArrayList<Integer> m = newShape.shape3D.get(i).get(j);
                        newShape.shape3D.get(i).set(j, newShape.shape3D.get(i).get(newShape.y-(p+1)));
                        newShape.shape3D.get(i).set(newShape.y-(p+1), m);
                        p++;
                    }
                }
                this.shape3D = newShape.shape3D;
                this.x = newShape.x;
                this.y = newShape.y;
                this.z = newShape.z;
                break;
            case 2:
                newShape = new Shape(this.x, this.z, this.y);
                for (int i = 0; i < this.x; i++) 
                    for (int j = 0; j < this.y; j++)
                        for (int k = 0; k < this.z; k++)
                            if (this.shape3D.get(i).get(j).get(k)== 1)
                                newShape.setPixel(i, k, j);
                for (int i = 0; i < newShape.y; i++) {
                    int p = 0;
                    for (int j = 0; j < newShape.z / 2; j++){
                        for (int k = 0; k < newShape.x; k++){
                            int m = newShape.shape3D.get(k).get(i).get(j);
                            newShape.shape3D.get(k).get(i).set(j, newShape.shape3D.get(k).get(i).get(newShape.z-(p+1)));
                            newShape.shape3D.get(k).get(i).set(newShape.z-(p+1), m);
                        }
                        p++;
                    }
                }
                this.shape3D = newShape.shape3D;
                this.x = newShape.x;
                this.y = newShape.y;
                this.z = newShape.z;
                break;
            case 3:
                newShape = new Shape(this.z, this.y, this.x);
                for (int i = 0; i < this.x; i++) 
                    for (int j = 0; j < this.y; j++)
                        for (int k = 0; k < this.z; k++)
                            if (this.shape3D.get(i).get(j).get(k)== 1)
                                newShape.setPixel(k, j, i);
                
                for (int i = 0; i < newShape.x; i++) {
                    int p = 0;
                    for (int j = 0; j < newShape.z / 2; j++){
                        for (int k = 0; k < newShape.y; k++){
                            int m = newShape.shape3D.get(i).get(k).get(j);
                            newShape.shape3D.get(i).get(k).set(j, newShape.shape3D.get(i).get(k).get(newShape.z-(p+1)));
                            newShape.shape3D.get(i).get(k).set(newShape.z-(p+1), m);
                        }
                        p++;
                    }
                }
                this.shape3D = newShape.shape3D;
                this.x = newShape.x;
                this.y = newShape.y;
                this.z = newShape.z;
                break;
        }
    }
    
    public void rotateCCW(int axis){
        switch(axis){
            case 1:
                rotateCW(1);
                rotateCW(1);
                rotateCW(1);
                break;
            case 2:
                rotateCW(2);
                rotateCW(2);
                rotateCW(2);
                break;
            case 3:
                rotateCW(3);
                rotateCW(3);
                rotateCW(3);
                break;
        }
    }
}
