/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

/**
 *
 * @author alexxx
 */
public class Task {
    public int length;
    public double angle;
    public int startX;
    public int startY;
    public double diffAngle1;
    public double diffAngle2;
    
    public int getLeftEndX(){
        int result = 0;
        result = startX - new Double(length*Math.cos(Math.PI-angle+diffAngle1)).intValue();
        return result;
    }
    
    public int getRightEndX(){
        int result = 0;
        result = startX - new Double(length*Math.cos(Math.PI-angle-diffAngle2)).intValue();
        return result;
    }
    
    public int getLeftEndY(){
        int result = 0;
        result = startY + new Double(length*Math.sin(Math.PI-angle+diffAngle1)).intValue();
        return result;
    }
    
    public int getRightEndY(){
        int result = 0;
        result = startY - new Double(length*Math.sin(Math.PI-angle-diffAngle2)).intValue();
        return result;
    }
    
    public int getNextLength(){
        return new Double(length/Math.sqrt(2)).intValue();
    }
    
    public double getLeftAngle(){
        return this.angle+diffAngle1;
    }
    
    public double getRightAngle(){
        return this.angle-diffAngle2;
    }
}
