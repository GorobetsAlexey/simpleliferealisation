/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class Neuron {
    
    public ArrayList<Double> weights = new ArrayList<Double>();
    public double limit = 9;
    
    public Neuron(int entersNum){
        for (int i = 0; i < entersNum; i++){
            this.weights.add(1.);
        }
    }
    
    public void train(ArrayList<Integer> set, boolean flag){
        if (flag){
        for (int i = 0; i < this.weights.size(); i++)
            this.weights.set(i, set.get(i)+this.weights.get(i));
        } else {
            for (int i = 0; i < this.weights.size(); i++)
            this.weights.set(i, this.weights.get(i)-set.get(i));
        }
    }
    
    public double getOut(ArrayList<Integer> set){
        double result = 0.;
        for (int i = 0; i < this.weights.size(); i++)
            result += this.weights.get(i)*set.get(i);
        return result;
    }
    
    public boolean answer(ArrayList<Integer> set){
        if (getOut(set) >= this.limit)
            return true;
        return false;
    }
    
    public String getIndexes(){
        String result = "";
        for (int i = 0; i < this.weights.size(); i++){
            result += this.weights.get(i);
            if (i < this.weights.size()-1)
                result += ";";
        }
        return result;
    }
    
}
