/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

import java.util.ArrayList;

/**
 *
 * @author alexxx
 */
public class NeuronNet {
    public ArrayList<Neuron> net = new ArrayList<Neuron>();
    
    public NeuronNet(int inputCount, int answerCount){
        for (int i = 0; i < answerCount; i++)
            this.net.add(new Neuron(inputCount));
    }
    
    public int getAnswer(ArrayList<Integer> set){
        int result = -1;
        for (int i = 0; i < this.net.size(); i++)
            if (this.net.get(i).answer(set))
                result = i;
        return result;
    }
    
    public void train(int answer, ArrayList<Integer> set){
        for (int i = 0; i < this.net.size(); i++){            
            if (i != answer) {
                if (this.net.get(i).answer(set))
                    this.net.get(i).train(set, false);
            } else {
                if (!this.net.get(i).answer(set))
                    this.net.get(i).train(set, true);
            }
        }
    }
}
