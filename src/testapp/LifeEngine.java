/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testapp;

/**
 *
 * @author alexxx
 */
public class LifeEngine {
    private LifeWorld world;
    
    public LifeEngine(LifeWorld world){
        this.world = world;
    }
    
    public void step(){
        LifeWorld next = this.world.clone();
        this.world.clear();
        for (int i = 0; i < this.world.sizeX(); i++){
            for ( int j = 0; j < this.world.sizeY(); j++){
                int n = next.getNeightboursCount(i, j);
                if (n == 3)
                    this.world.set(i, j, true);
                if (n == 2 && next.get(i, j))
                    this.world.set(i, j, true);
                if (n < 2 || n > 3)
                    this.world.set(i, j, false);
            }
        }
    }
}
